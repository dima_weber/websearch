#include "searchresultsmodel.h"
#include <QColor>

SearchResultsModel::SearchResultsModel(QObject *parent)
    :QAbstractItemModel(parent)
{
}

QModelIndex SearchResultsModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid())
        return createIndex(row, column, parent.row());
    else
        return createIndex(row, column, -1);
}

QModelIndex SearchResultsModel::parent(const QModelIndex &index) const
{
    if (index.internalId() == -1)
        return QModelIndex();
    else
        return createIndex(index.internalId(), 0, -1);
}

int SearchResultsModel::rowCount(const QModelIndex &parent) const
{
    if (parent == QModelIndex())
        return searchResults.size();
    else if (parent.parent() == QModelIndex() && parent.column() == 0)
    {
        const DataItem* item = searchResults[parent.row()].get();
        return item->finds.count();
    }
    else
        return 0;
}

int SearchResultsModel::columnCount(const QModelIndex &parent) const
{
    if (parent == QModelIndex())
        return 3;
    else
        return 1;
}

QVariant SearchResultsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        if (index.parent() == QModelIndex())
        {
            const DataItem* item = searchResults[index.row()].get();
            switch (index.column())
            {
            case 0:
                return item->url.toString();
            case 1:
                return item->status;
            case 2:
                return item->finds.count();
            }
        }
        else if (index.column() == 0)
            return searchResults[index.parent().row()]->finds[index.row()];
    }

    if (role == Qt::ToolTipRole)
    {
        if (index.parent() == QModelIndex())
        {
            const DataItem* item = searchResults[index.row()].get();
            if (index.column() == 1)
                return item->strStatus;
        }
    }

    if (role == Qt::ForegroundRole)
    {
        if (!index.parent().isValid())
        {
            const DataItem* item = searchResults[index.row()].get();
            if (item->status == 200)
                return QColor("green");
            else if (item->status == 0)
                return QColor("gray");
            else
                return QColor("red");
        }
    }

    return QVariant();
}

bool SearchResultsModel::hasChildren(const QModelIndex& index) const
{
    if (index == QModelIndex())
        return !searchResults.isEmpty();

    if (index.column() > 0)
        return false;

    if (index.parent() == QModelIndex())
    {
        const DataItem* item = searchResults[index.row()].get();
        return !item->finds.isEmpty();
    }
    else
        return false;
}

QVariant SearchResultsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Vertical)
        return section;

    if (orientation == Qt::Horizontal)
        switch(section)
        {
            case 0: return "URL";
            case 1: return "status";
            case 2: return "count";
        }

    return QVariant();
}

void SearchResultsModel::addNewWebPage(const QUrl &string)
{
    QMutexLocker lock(&accessMutex);

    if (string.isEmpty())
        return;

    beginInsertRows(QModelIndex(), searchResults.count(), searchResults.count()+1);
    DataItem::Ptr item = std::make_shared<DataItem>();
    item->url = string;
    item->status = 0;
    item->row = searchResults.count();
    searchResults << item;
    searchResultsMap[string] = item;
    endInsertRows();
}

void SearchResultsModel::modifyWebPageStatus(const QUrl &string, int status, const QString& strCode)
{
    QMutexLocker lock(&accessMutex);

    if (!searchResultsMap.contains(string))
        return;

    DataItem::Ptr item = searchResultsMap[string];
    item->status = status;
    item->strStatus = strCode;
    emit dataChanged(createIndex(item->row, 0, -1), createIndex(item->row, 3, -1), {Qt::DisplayRole, Qt::ForegroundRole});
}

void SearchResultsModel::addWebPageFinding(const QUrl &string, const QString &str)
{
    QMutexLocker lock(&accessMutex);

    if (!searchResultsMap.contains(string))
        return;

    DataItem::Ptr item = searchResultsMap[string];
    QModelIndex idx = createIndex(item->row, 0, -1);
    beginInsertRows(idx, item->finds.count(), searchResults.count()+1);
    item->finds << str;
    endInsertColumns();
    emit dataChanged(createIndex(item->row, 0, -1), createIndex(item->row, 2, -1), {Qt::DisplayRole});
}

void SearchResultsModel::clear()
{
    QMutexLocker lock(&accessMutex);

    beginResetModel();
    searchResultsMap.clear();
    searchResults.clear();
    endResetModel();
}
