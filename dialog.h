#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QThread>
#include <QPointer>
#include <QAtomicInt>

#include "searcher.h"

namespace Ui {
class Dialog;
}

class SearchResultsModel;

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

signals:
    void startRequested();
    void stopRequested();
    void pauseRequested();
    void resumeRequested();

public slots:
    void addWebPage(const QUrl &string);
    void modifyWebPageStatus(const QUrl &string, int status, const QString &strCode);
    void addWebPageFinding(const QUrl &string, const QString& str);

    void onSearcherStarted();
    void onSearcherStopped();

private slots:
    void onStartStopPress();
    void onPausePressed();
    void onParametersChanges();

    void recreateThreads();
    void onWaitingWorkers(int);

private:
    enum State {Idle, Running, Paused};
    Ui::Dialog *ui;
    State state;
    SearchResultsModel* pSearchResultsModel;
    QVector<QPair<QPointer<Searcher>, QPointer<QThread>>> searchPool;
    UrlQueue queue;
    QAtomicInt runningThreads;

    void startSearch();
    void stopSearch();

};

#endif // DIALOG_H
