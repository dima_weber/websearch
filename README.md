# README #


### What is this repository for? ###

Thie is demo program for multithread web-crawling.
Version 1.0


## Install / run notes ##
### Linux (original development) ###

* Qt5 (5.7) required
* Please install libhtmlcxx package to run program (sudo apt-get install libhtmlcxx3v5 on Ubuntu)
* Please install libhtmlcxx-dev package to build program (sudo apt-get install libhtmlcxx-dev on Ubuntu)
* No any other external dependencies

### Windows (port)###

* Qt5 (5.7) required
* You can find includes and x64 libraries for htmlcxx in  repository, pro file configured to use it
* You can compile own htmlcxx in Visual Studion, sources can be found at http://htmlcxx.sourceforge.net/
* Please make sure application can find libeay32.dll and ssleay32.dll, otherwise https would not be working.

### Testing ###

* Tests was running on wikipedia.org, searching word like 'wiki', 'London', 'пицца'. Make sure you set max sites big enought.
* Due to unpredictable multithreading results (you never know, which threads process their urls and put suburl in queue first) -- different runs can give  different results.

### TODO ###
* make single QNetworkAccessManager as separate thread and let him send results back to worker threads
* use curl as alternative to QNAM
* add Proxy model to show only non-zero results
* make Thread Count field enabled while processing -- there are no obvious limitations to dynamic threads number
* optimize Model behaviour on large elements number