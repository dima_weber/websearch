#include "dialog.h"
#include "ui_dialog.h"
#include "searchresultsmodel.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog),
    state(Idle),
    runningThreads(0)
{
    ui->setupUi(this);

    this->setLayout(ui->verticalLayout);
    ui->pPauseButton->setVisible(false);

    pSearchResultsModel = new SearchResultsModel(this);
    ui->pTreeView->setModel(pSearchResultsModel);
    ui->pTreeView->header()->setSectionResizeMode(0, QHeaderView::ResizeMode::Stretch);
    ui->pTreeView->header()->setSectionResizeMode(1, QHeaderView::ResizeMode::ResizeToContents);
    ui->pTreeView->header()->setSectionResizeMode(2, QHeaderView::ResizeMode::ResizeToContents);
    ui->pTreeView->header()->setStretchLastSection(false);

    connect (ui->pThreadsCountSpin, SIGNAL(valueChanged(int)), SLOT(recreateThreads()));
    connect (&queue, SIGNAL(waitingWorkersCount(int)), this, SLOT(onWaitingWorkers(int)));

    connect (ui->pStartStopButton, SIGNAL(clicked(bool)), this, SLOT(onStartStopPress()));
    connect (ui->pPauseButton, SIGNAL(clicked(bool)), this, SLOT(onPausePressed()));
    connect (ui->pStartUrlEdit, SIGNAL(textChanged(QString)), this, SLOT(onParametersChanges()));
    connect (ui->pTextToSearchEdit, SIGNAL(textChanged(QString)), this, SLOT(onParametersChanges()));

    recreateThreads();
    stopSearch();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::addWebPage(const QUrl &string)
{
    pSearchResultsModel->addNewWebPage(string);
}

void Dialog::modifyWebPageStatus(const QUrl &string, int status, const QString& strCode)
{
    pSearchResultsModel->modifyWebPageStatus(string, status, strCode);

    ui->pProgressBar->setValue(ui->pProgressBar->value() + 1);
}

void Dialog::addWebPageFinding(const QUrl &string, const QString &str)
{
    pSearchResultsModel->addWebPageFinding(string, str);
}

void Dialog::onSearcherStarted()
{
    runningThreads++;
}

void Dialog::onSearcherStopped()
{
    runningThreads--;
    if (runningThreads == 0)
        stopSearch();
}

void Dialog::onStartStopPress()
{
    if (state == Idle)
    {
        startSearch();
        emit startRequested();
    }
    else if (state == Running || state == Paused)
    {
        stopSearch();
        emit stopRequested();
    }
}

void Dialog::onPausePressed()
{
    if (state == Running)
    {
        state = Paused;
        ui->pPauseButton->setChecked(true);
        ui->pPauseButton->setText("Resume");
        emit pauseRequested();
    }
    else if (state==Paused)
    {
        state = Running;
        ui->pPauseButton->setChecked(false);
        ui->pPauseButton->setText("Pause");
        emit resumeRequested();
    }
}

void Dialog::onParametersChanges()
{
    ui->pStartStopButton->setEnabled( !(ui->pStartUrlEdit->text().isEmpty() || ui->pTextToSearchEdit->text().isEmpty() ) );
}

void Dialog::startSearch()
{
    state = Running;
    ui->pStartStopButton->setText("Stop");
    pSearchResultsModel->clear();

    QUrl url;
    url.setUrl(ui->pStartUrlEdit->text());
    if (url.scheme().isEmpty())
        url.setScheme("http://");

    ui->pProgressBar->setRange(0, ui->pMaxPagesCountSpin->value());
    ui->pProgressBar->setValue(0);
    ui->pPauseButton->setVisible(state != Idle);
    ui->pStartUrlEdit->setEnabled(state == Idle);
    ui->pTextToSearchEdit->setEnabled(state == Idle);
    ui->pMaxPagesCountSpin->setEnabled(state == Idle);
    ui->pThreadsCountSpin->setEnabled(state == Idle);
    ui->pCaseSensitive->setEnabled(state == Idle);
    ui->pNoHttps->setEnabled(state==Idle);
    ui->pFollowRedirect->setEnabled(state == Idle);

    for(QPair<QPointer<Searcher>, QPointer<QThread>> p: searchPool)
    {
        p.first->setCaseSensitive(ui->pCaseSensitive->isChecked());
        p.first->setFollowRedirect(ui->pFollowRedirect->isChecked());
        p.first->setSearchedText(ui->pTextToSearchEdit->text());
    }

    queue.clear();
    queue.setMaxCount(ui->pMaxPagesCountSpin->value());
    queue.setNoHttps(ui->pNoHttps->isChecked());
    queue.putUrl(url);
}

void Dialog::stopSearch()
{
    state = Idle;
    ui->pStartStopButton->setText("Start");
    ui->pPauseButton->setChecked(false);
    ui->pPauseButton->setText("Pause");

    ui->pPauseButton->setVisible(state != Idle);
    ui->pStartUrlEdit->setEnabled(state == Idle);
    ui->pTextToSearchEdit->setEnabled(state == Idle);
    ui->pMaxPagesCountSpin->setEnabled(state == Idle);
    ui->pThreadsCountSpin->setEnabled(state == Idle);
    ui->pCaseSensitive->setEnabled(state == Idle);
    ui->pFollowRedirect->setEnabled(state == Idle);
    ui->pNoHttps->setEnabled(state==Idle);

    queue.clear();
}

void Dialog::recreateThreads()
{
    if (ui->pThreadsCountSpin->value() < searchPool.count())
    {
        while (ui->pThreadsCountSpin->value() < searchPool.count())
        {
            QPair<QPointer<Searcher>, QPointer<QThread>> pair = searchPool.takeLast();
            pair.first->stop();
            pair.second->quit();
            pair.first->deleteLater();
            pair.second->deleteLater();
        }
    }
    else if (ui->pThreadsCountSpin->value() > searchPool.count())
    {
        while(ui->pThreadsCountSpin->value() > searchPool.count())
        {
            QPointer<Searcher> search = new Searcher(&queue);
            QPointer<QThread> thread = new QThread();
            thread->setObjectName("Searcher thread");
            searchPool.append(QPair<QPointer<Searcher>, QPointer<QThread>>(search, thread));

            search->moveToThread(thread);
            connect(search, SIGNAL(pageStarted(const QUrl&)), SLOT(addWebPage(const QUrl&)));
            connect(search, SIGNAL(pageDownloaded(const QUrl&, int, const QString&)), SLOT(modifyWebPageStatus(const QUrl&, int, const QString&)));
            connect(search, SIGNAL(foundOnPage(const QUrl&, const QString&)), SLOT(addWebPageFinding(const QUrl&, const QString&)));

            connect (this, SIGNAL(startRequested()), thread, SLOT(start()));
            connect (search, SIGNAL(stopped()), thread, SLOT(quit()));
            connect (this, SIGNAL(stopRequested()), search, SLOT(stop()),  Qt::DirectConnection);

            connect (thread, SIGNAL(started()), search, SLOT(start()));
            connect (search, SIGNAL(started()), this, SLOT(onSearcherStarted()));
            connect (search, SIGNAL(stopped()), this, SLOT(onSearcherStopped()));

            connect (this, SIGNAL(pauseRequested()), search, SLOT(pause()), Qt::DirectConnection);
            connect (this, SIGNAL(resumeRequested()), search, SLOT(resume()), Qt::DirectConnection);
        }
    }

    qDebug() << "there are " << searchPool.count() <<"thread now";
}

void Dialog::onWaitingWorkers(int waitingInQueue)
{
    if (waitingInQueue == runningThreads)
        queue.clear();
}
