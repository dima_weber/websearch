#ifndef SEARCHRESULTSMODEL_H
#define SEARCHRESULTSMODEL_H

#include <QObject>
#include <QModelIndex>
#include <QVector>
#include <QMutex>
#include <QUrl>

#include <memory>
class SearchResultsModel : public QAbstractItemModel
{
    Q_OBJECT
    QMutex accessMutex;

public:
    SearchResultsModel(QObject* parent = nullptr);

    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &index) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool hasChildren(const QModelIndex& index) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;


public slots:
    void addNewWebPage(const QUrl& string);
    void modifyWebPageStatus(const QUrl& string, int status, const QString &strCode);
    void addWebPageFinding(const QUrl &string, const QString& str);

    void clear();
private:
    struct DataItem
    {
        typedef std::shared_ptr<DataItem> Ptr;
        QUrl url;
        int status;
        QString strStatus;
        QStringList finds;
        int row;
    };

    QVector<DataItem::Ptr> searchResults;
    QMap<QUrl, DataItem::Ptr> searchResultsMap;
};

#endif // SEARCHRESULTSMODEL_H
