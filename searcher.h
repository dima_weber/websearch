#ifndef SEARCHER_H
#define SEARCHER_H

#include <QtCore/QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMutex>

#ifdef USE_HTMLCXX
#   include <htmlcxx/html/ParserDom.h>
#endif

#include <QPointer>
#include <QWaitCondition>

#include <memory>

class UrlQueue : public QObject
{
    Q_OBJECT
private:
    QList<QUrl> subUrls;
    QList<QUrl> parsedUrls;
    int maxCount;
    QMutex newTaskMutex;
    QMutex accessListsMutex;
    QWaitCondition newTaskAlarm;
    int waitingWorkers;
    bool noHttps;
public:
    UrlQueue(QObject* parent = nullptr);
    QUrl retrieveUrl();
    void setMaxCount(int cnt) { maxCount = cnt;}
    bool hasUrls() const {return !subUrls.isEmpty();}
    void setNoHttps(bool no) { noHttps = no;}
public slots:
    bool putUrl(const QUrl& url);
    void clear();

signals:
    void waitingWorkersCount(int);
};

class Searcher : public QObject
{
    Q_OBJECT
    QUrl url;
    bool followRedirect;
    QString text;
    bool caseSensitive;
    QPointer<QNetworkAccessManager> manager;
# ifdef USE_HTMLCXX
    std::unique_ptr<tree<htmlcxx::HTML::Node>> pDom;
#else
    QString html;
#endif
    QMutex statusMutex;
    QWaitCondition pauseWait;
    UrlQueue* queue;
    enum Status {WaitingUrl, Running, Paused, Done};
    Status status;

public:
    explicit Searcher(UrlQueue* queue, QObject *parent = 0);

    void setUrl(const QString& url){this->url = url;}
    void setFollowRedirect(bool follow=true);
    void setSearchedText(const QString& text);
    void setCaseSensitive(bool sensitive);

signals:
    void pageStarted(const QUrl& url);
    void pageDownloaded(const QUrl& url, int status, const QString& strCode);
    void foundOnPage(const QUrl& url, const QString& finding);
    void linkFound(const QUrl& url);

    void pageDone(const QUrl& url);
    void stopped();
    void started();

public slots:
    void processNextUrl();
    void start();
    void stop();
    void pause();
    void resume();

private slots:
    void onPage();
    void onError(QNetworkReply::NetworkError);
    void onSslError(const QList<QSslError> &errors);

private:
    void parseHtml(QIODevice* source);
    void parseVisibleText();
    void parseLinks();

    bool shouldContinue();
};

#endif // SEARCHER_H
