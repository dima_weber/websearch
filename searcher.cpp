#include "searcher.h"

#include <QNetworkRequest>
#include <QDebug>
#include <QMutexLocker>
#include <QThread>

Searcher::Searcher(UrlQueue* queue, QObject *parent) : QObject(parent), manager(new QNetworkAccessManager(this)), queue(queue)
{
    status = Done;
    connect (this, SIGNAL(pageDone(QUrl)), SLOT(processNextUrl()));
}

void Searcher::setFollowRedirect(bool follow)
{
    followRedirect = follow;
}

void Searcher::setSearchedText(const QString &text)
{
    this->text = text;
}

void Searcher::setCaseSensitive(bool sensitive)
{
    this->caseSensitive = sensitive;
}

void Searcher::processNextUrl()
{
    url = queue->retrieveUrl();

    if (url.isEmpty())
    {
        statusMutex.lock();
        status = Done;
        statusMutex.unlock();
        emit stopped();
        return;
    }

    statusMutex.lock();
    status = Running;
    statusMutex.unlock();

    qDebug() << "thread " << QThread::currentThreadId() << "process url" << url.toString() << url.toString().toUtf8();

    emit pageStarted(url);

    QNetworkRequest req;
    req.setUrl(url);
    req.setRawHeader("User-Agent", "WebSearch 1.0");
    req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, followRedirect);

    QNetworkReply* reply = manager->get(req);
    connect(reply, SIGNAL(finished()), SLOT(onPage()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), SLOT(onError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(const QList<QSslError> &)), SLOT(onSslError(const QList<QSslError> &)));
}

void Searcher::start()
{
    statusMutex.lock();
    status = WaitingUrl;
    statusMutex.unlock();
    QThread::currentThread()->setPriority(QThread::LowPriority);
    emit started();
    processNextUrl();
}

void Searcher::stop()
{
    pauseWait.wakeAll();
    QMutexLocker lock(&statusMutex);
    status = Done;
}

void Searcher::pause()
{
    statusMutex.lock();
    status = Paused;
    statusMutex.unlock();
}

void Searcher::resume()
{
    statusMutex.lock();
    status = Running;
    statusMutex.unlock();
    pauseWait.wakeAll();
}

void Searcher::onPage()
{
    if (!shouldContinue())
        return;

    QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
    if (!reply)
        return;

    int httpCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QString httpCodeStr = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    emit pageDownloaded(url, httpCode, httpCodeStr);

    if (httpCode == 200)
    {

        parseHtml(reply);

        parseLinks();
        if (!shouldContinue())
            return;

        parseVisibleText();
        if (!shouldContinue())
            return;

    }
    emit pageDone(url);
    statusMutex.lock();
    status = WaitingUrl;
    statusMutex.unlock();

    reply->deleteLater();
}

void Searcher::onError(QNetworkReply::NetworkError)
{
    if (url.isEmpty())
        return;

    QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
    if (!reply)
        return;

    int httpCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QString httpCodeStr = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    emit pageDownloaded(url, httpCode, httpCodeStr);

    emit pageDone(url);
    statusMutex.lock();
    status = WaitingUrl;
    statusMutex.unlock();

    reply->deleteLater();
}

void Searcher::onSslError(const QList<QSslError> &errors)
{
    QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
    if (!reply)
        return;

    int httpCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QString httpCodeStr = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    emit pageDownloaded(url, httpCode, httpCodeStr);

    emit pageDone(url);
    statusMutex.lock();
    status = WaitingUrl;
    statusMutex.unlock();

    reply->deleteLater();
}

void Searcher::parseHtml(QIODevice* source)
{
    QByteArray data = source->readAll();
    QString html = QString::fromUtf8(data);

#ifdef USE_HTMLCXX
    htmlcxx::HTML::ParserDom parser;
    pDom.reset(new tree<htmlcxx::HTML::Node>(parser.parseTree(html.toStdString())));
#else
    this->html = html;
#endif
}

void Searcher::parseVisibleText()
{
    QString visibleText;
#ifdef USE_HTMLCXX
    tree<htmlcxx::HTML::Node>::iterator it = pDom->begin();
    tree<htmlcxx::HTML::Node>::iterator end = pDom->end();
    for (; it != end; ++it)
    {
        if (!shouldContinue())
            return;

        if ((!it->isTag()) && (!it->isComment()))
        {
            visibleText +=  QString::fromStdString(it->text());
        }
    }
#else
    visibleText = html;
#endif

    visibleText = visibleText.simplified();
    int pos = -text.length();
    do
    {
        if (!shouldContinue())
            return;

        pos = visibleText.indexOf(text, pos + text.length(), caseSensitive?Qt::CaseSensitive:Qt::CaseInsensitive);
        if (pos > 0)
        {
            emit foundOnPage(url.toString(), visibleText.mid(pos-50, 100 + text.length()));
        }
    } while (pos > -1);
}

void Searcher::parseLinks()
{
#ifdef USE_HTMLCXX
    tree<htmlcxx::HTML::Node>::iterator it = pDom->begin();
    tree<htmlcxx::HTML::Node>::iterator end = pDom->end();
    for (; it != end; ++it)
    {
        if (!shouldContinue())
            return;

        QString tag = QString::fromStdString(it->tagName()).toLower();
        if (tag == "a")
        {
            it->parseAttributes();
            std::pair<bool, std::string> p = it->attribute("href");
            if (p.first)
            {
                QString link = QString::fromStdString(p.second);
                QUrl subUrl = link;
                if (subUrl.scheme().isEmpty())
                    subUrl.setScheme(url.scheme());
                if (subUrl.host().isEmpty())
                    subUrl.setHost(url.host());
                //emit linkFound(subUrl);
                queue->putUrl(subUrl);
            }
        }
    }
#else
    int pos = 0;
    QRegExp linkRx("http://[^\\s]*");
    while((pos = linkRx.indexIn(html, pos)) > -1)
    {
        QString link = linkRx.cap(0);
        QUrl subUrl = link;
        if (subUrl.scheme().isEmpty())
            subUrl.setScheme(url.scheme());
        if (subUrl.host().isEmpty())
            subUrl.setHost(url.host());
        //emit linkFound(subUrl);
        queue->putUrl(subUrl);
        pos +=  link.length();
    }
#endif
}

bool Searcher::shouldContinue()
{
    QMutexLocker lock(&statusMutex);
    if(status == Paused)
        pauseWait.wait(&statusMutex);
    if (status == Done)
    {
        emit stopped();
        return false;
    }
    return true;
}

bool UrlQueue::putUrl(const QUrl &url)
{
    if (url.host().isEmpty())
        return false;

    if (noHttps && url.scheme() == "https")
        return false;

    QMutexLocker lock(&accessListsMutex);
    if (parsedUrls.count() + subUrls.count() >= maxCount)
        return false;

    if (parsedUrls.contains(url) || subUrls.contains(url))
        return false;

    qDebug() << QThread::currentThreadId() << " putting  " << url << "OK";

    subUrls.append(url);
    newTaskAlarm.wakeOne();
    return true;
}

UrlQueue::UrlQueue(QObject *parent)
    :QObject (parent),waitingWorkers(0)
{
}

QUrl UrlQueue::retrieveUrl()
{
    QUrl url;
    QMutexLocker lock(&newTaskMutex);
    if (parsedUrls.count() + subUrls.count() >= maxCount && !hasUrls())
    {
        // No more urls will be added -- we reach max url count. And there are no unprocessed urls.
        // Worker can just quit
        return QUrl();
    }

    if (!hasUrls())
    {
        // No unprocessed urls -- wait for any
        waitingWorkers++;
        emit waitingWorkersCount(waitingWorkers);
        newTaskAlarm.wait(&newTaskMutex);
        waitingWorkers--;
    }
    if (!hasUrls())
    {
        // Even after someone reported there are an url added -- still no urls. For example -- queue is full
        // Worker can just quit
        url =  QUrl();
    }
    else
    {
        // New url found in queue -- give it for processing
        QMutexLocker lock(&accessListsMutex);
        url = subUrls.takeFirst();
        parsedUrls.append(url);
    }

    return url;
}


void UrlQueue::clear()
{
    {
        QMutexLocker lock(&accessListsMutex);
        subUrls.clear();
        parsedUrls.clear();
    }
    newTaskAlarm.wakeAll();
}

